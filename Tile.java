public enum Tile{
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C"),
	BLANK("_");
	
	private String name;
	private Tile(String tile){
		this.name = tile;
	}
	private String getName(){
		return this.name;
	}
	public String toString() {
        return this.name;
    }
}
	