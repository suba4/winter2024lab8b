import java.util.Scanner;
public class BoardGameApp{
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// System.out.println(playerToken);
		Board board = new Board(); 
		
		System.out.println("Welcome to my Game");
		int numCastles = 7;
		int turns = 0;
		int placeTokenVal = 0;
		int row = 0;
		int col = 0;
		while(numCastles > 0 && turns < 8){	
			System.out.println(board);
			System.out.println("Number of Castles is " + numCastles);
			System.out.println("Number of turns is " + turns);
		
			System.out.println("Please enter row number");
			row = Integer.parseInt(reader.nextLine());
			System.out.println("Please enter col number");
			col = Integer.parseInt(reader.nextLine());
			
			placeTokenVal = board.placeToken(row,col);
			while(placeTokenVal < 0){
				System.out.println("Please enter valid row and col number");
				
				System.out.println("Please enter row number");
				row = Integer.parseInt(reader.nextLine());
				System.out.println("Please enter col number");
				col = Integer.parseInt(reader.nextLine());
				
				placeTokenVal = board.placeToken(row,col);
				
			}
			if(placeTokenVal == 1){
				System.out.println("There is a wall at this position");
				turns++;
			}	
			else{
				System.out.println("A castle was placed");
				turns++;
				numCastles--;
			}
		
			
		
		}
		
		if(numCastles == 0){
			System.out.println("You won, Congragulations");
		}
		else{
			System.out.println("You lost, Unfortunately");
		}
	}
	
}