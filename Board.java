import java.util.Random;
public class Board{
	private Tile[][] grid;
	private final int gameLength = 5;
	private Random rng;
	
	public Board(){
		this.grid = new Tile[this.gameLength][this.gameLength]; 
		rng = new Random();
		for (int i = 0; i < grid.length ; i++){
			int randIndex = rng.nextInt(grid[i].length);
			for(int j = 0; j < grid[i].length; j++){
				if(j == randIndex){
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else{
					grid[i][j] = Tile.BLANK;
				}
			}
		}		
	}
	public String toString(){
		String all = "";
		for (int i = 0; i < grid.length ; i++){
			for(int j = 0; j < grid[i].length; j++){
				all += grid[i][j] + " ";
			}
		all += "\n";
		}	
		return all;
	}


	public int placeToken(int row, int col) {
        int input = 0;
		if (row < 0 || row > gameLength-1 || col < 0 || col > gameLength-1) {
            input = -2;
        }
        else if(grid[row][col].equals(Tile.CASTLE) || grid[row][col].equals(Tile.WALL)){
			input = -1;
		}
		else if(grid[row][col].equals(Tile.HIDDEN_WALL)){
			input = 1;
			grid[row][col].equals(Tile.WALL);
		}
		else{
			input = 0;
			grid[row][col].equals(Tile.CASTLE);
		}
        return input;
    }

}
